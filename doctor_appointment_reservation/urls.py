from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path

from panel import views
from django.conf import settings
from django.contrib import admin

admin.autodiscover()
# admin.site.login = login_required(admin.site.login)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/login/', views.LoginView.as_view(), name='panel-login-api'),
    path('api/logout/', views.logout_view, name='panel-logout-api'),
    path('api/register/', views.RegisterView.as_view(), name='panel-register-api'),

    path('api/get_available_times/', views.get_available_times),
    path('api/set_appointment/', views.set_appointment),

    path('api/post_comment/', views.post_comment),
    path('api/filter/', views.get_doctors_by_filter, name='panel-filter'),
    path('api/add_favorite_doctor/', views.add_to_favourite_doctors),
    path('api/get_users_favourite_doctors/', views.get_users_favourite_doctors),
    path('api/update_patient_info/', views.update_patient_info, name='panel-patient-info'),
    path('api/get_doctor_comments/', views.get_doctor_comments, name='panel-doctor-comments'),
    path('api/get_doctor_comments_by_patient/', views.get_doctor_comments_by_patient, name='panel-patient-comments'),
    path('api/get_comments_of_doctor/', views.get_comments_of_doctor, name='panel-doctor-comments_posted'),
    path('api/get_patient_appointments/', views.get_patient_appointments, name='panel-patient-appointments'),
    path('api/get_doctor_appointments/', views.get_doctor_appointments, name='panel-doctor-appointments'),

    # path('api/update_working_hours/', views.update_working_hours),
    path('api/doctor_profile/', views.doctor_profile_view, name='panel-doctor-profile'),
    path('api/patient_profile/', views.patient_profile_view, name='panel-patient-profile'),
    path('api/update_doctor_info/', views.update_doctor_info, name='panel-doctor-info'),

]
