from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from .models import Patient,Doctor as user


def is_email_valid(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def validate_mail(email):
    if user.objects.filter(**{'{}__iexact'.format(user.USERNAME_FIELD): email}).exists():
        raise ValidationError('User with this {} already exists'.format(user.USERNAME_FIELD))
    elif not is_email_valid(email):
        raise ValidationError('invalid {}'.format(user.USERNAME_FIELD))
    return email



def is_doctor_id_valid(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def validate_doctor_id(doctor_id):
    if user.objects.filter(**{'{}__iexact'.format(user.doctor_ID): doctor_id}).exists():
        raise ValidationError('doctor with this {} already exists'.format(user.doctor_ID))
    elif not is_doctor_id_valid(doctor_id):
        raise ValidationError('invalid {}'.format(user.doctor_ID))
    return doctor_id