from datetime import datetime

from django.shortcuts import render
import json
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.contrib.auth import login, logout, authenticate
from rest_framework import views, permissions, authentication
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response, SimpleTemplateResponse
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView
from django.shortcuts import render, redirect
# from .forms import AppointmentForm

from .models import Appointment, Doctor, Comment, AvailableTime, Patient
from .serializers import LoginSerializer, DoctorProfileSerializer, PatientProfileSerializer, RegisterSerializer


# todo httpresponse haro bayad doros konim
# request.user ro bayad doros konim ke chizaye marbut be maro bede


class CsrfExemptSessionAuthentication(authentication.SessionAuthentication):
    def enforce_csrf(self, request):
        return


class LoginView(views.APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        serializer = LoginSerializer()
        return render(request, template_name='login.html', context={'form': serializer}, status=200)

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data
            # user.backend = settings.AUTHENTICATION_BACKENDS[0]
            login(request, user)

            return Response({'message': 'Login Successful'}, status=200)

        except ValidationError as e:
            return Response({'message': e.detail.get('non_field_errors')[0]})


class RegisterView(views.APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    #
    # def get(self, request):
    #     if request.user.is_authenticated:
    #         # todo
    #         pass
    #     serializer = RegisterSerializer()
    #     return Response({'form': serializer})

    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({'message': 'Bad Credentials'}, status=400)
        user = serializer.create(serializer.validated_data)
        # user.backend = settings.AUTHENTICATION_BACKENDS[0]
        login(self.request, user)

        return Response({'message': 'Registered Successfully'}, status=200)


@api_view(['GET'])
@login_required()
def doctor_profile_view(request):
    serializer = DoctorProfileSerializer()
    serializer.fields['name'].initial = request.user.name
    serializer.fields['last_name'].initial = request.user.last_name
    serializer.fields['phone'].initial = request.user.phone
    serializer.fields['city'].initial = request.user.city
    return HttpResponse({'form': serializer}, status=200)


@api_view(['GET'])
@login_required()
def patient_profile_view(request):
    serializer = PatientProfileSerializer()
    serializer.fields['name'].initial = request.user.name
    serializer.fields['last_name'].initial = request.user.last_name
    serializer.fields['phone'].initial = request.user.phone
    return HttpResponse({'form': serializer}, status=200)


@login_required()
@api_view(['POST'])
@authentication_classes([CsrfExemptSessionAuthentication])
def update_doctor_info(request):
    user = request.user
    if request.user.is_authenticated:
        if hasattr(user, 'doctor'):
            user = user.doctor
            serializer = DoctorProfileSerializer(data=request.data)
            if not serializer.is_valid():
                return Response({'message': 'Bad Credentials'}, status=400)
            serializer.update(user, serializer.validated_data)
            return Response({'message': 'update successful', 'updated_info': serializer.validated_data}, status=200)
        else:
            return Response({'message': 'You are not a doctor'}, status=403)
    else:
        return Response({'message': 'Unauthorized access'}, status=401)


@login_required(login_url='/api/login')
@api_view(['POST'])
@authentication_classes([CsrfExemptSessionAuthentication])
def update_patient_info(request):
    user = request.user
    if request.user.is_authenticated:
        if hasattr(user, 'patient'):
            user = user.patient
            serializer = PatientProfileSerializer(data=request.data)
            if not serializer.is_valid():
                return Response({'message': 'Bad Credentials'}, status=400)
            serializer.update(user, serializer.validated_data)
            return Response({'message': 'update successful', 'updated_info': serializer.validated_data}, status=200)
        else:
            return Response({'message': 'You are not a patient'}, status=403)
    else:
        return Response({'message': 'Unauthorized access'}, status=401)


@api_view(['GET'])
def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
    return HttpResponse(status=200)


@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_doctor_appointments(request):
    appointment_find = Appointment.objects.filter(doctor=Doctor.objects.get(id=request.user.id))
    if appointment_find.count() != 0:
        return Response(list(appointment_find.values('patient', 'description', 'start')), status=200)
    else:
        return Response({"message":"no appointment for you"}, status=200)


@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_patient_appointments(request):
    appointment_find = Appointment.objects.filter(patient=Patient.objects.get(id=request.user.id))
    return Response(list(appointment_find.values('doctor', 'description', 'start')), status=200)


@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_doctor_comments(request):
    comments_find = Comment.objects.filter(doctor=Doctor.objects.get(id=request.user.id))
    return Response(list(comments_find.values('text', 'patient')), status=200)


@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_doctor_comments_by_patient(request):
    comments_find = Comment.objects.filter(patient=Patient.objects.get(id=request.user.id))
    return Response(list(comments_find.values('text', 'doctor')), status=200)


@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_comments_of_doctor(request):
    comments_find = Comment.objects.filter(doctor=Doctor.objects.get(council_ID=request.data['council_id']))
    return Response(list(comments_find.values('text')), status=200)


@api_view(['GET'])
@authentication_classes([CsrfExemptSessionAuthentication])
def get_doctors_by_filter(request):
    # qcity , qdegree, qspecialty,qlastname
    doctors = Doctor.objects.all()
    qcity = request.GET.get('qcity')
    qdegree = request.GET.get('qdegree')
    qspecialty = request.GET.get('qspecialty')
    qlastname = request.GET.get('qlastname')
    qcouncil_id = request.GET.get('qcouncil_id')
    if qcity:
        doctors = doctors.filter(city=qcity)
    if qdegree:
        doctors = doctors.filter(degree=qdegree)
    if qspecialty:
        doctors = doctors.filter(specialty__icontains=qspecialty)
    if qlastname:
        doctors = doctors.filter(last_name__icontains=qlastname)
    if qcouncil_id:
        doctors = doctors.filter(council_ID=qcouncil_id)
    return Response(
        list(doctors.values('first_name', 'last_name', 'degree', 'specialty', 'city', 'phone', 'council_ID')),
        status=200)


# @api_view("POST")
# @login_required()
# @authentication_classes([CsrfExemptSessionAuthentication])
# def new_appointment(request):
#     if request.method == 'POST':
#         form = AppointmentForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('')
#     else:
#         form = AppointmentForm()
#     return render(request, 'appointment_form.html', {'form': form})
#

weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]


def find_available_times(doctor, date):
    from datetime import timedelta
    from .models import appointment_duration
    weekday_str = weekdays[date.date().weekday()]
    try:
        available_day = doctor.available_times.all().get(day=weekday_str)
    except:
        return None
    day_appointments = Appointment.objects.filter(doctor=doctor, start__date=date)
    day_appointments.order_by('start')

    visit_begins_list = []
    cur = date.replace(hour=available_day.start_time.hour, minute=available_day.start_time.minute)
    end = date.replace(hour=available_day.end_time.hour, minute=available_day.end_time.minute)

    while cur < end:
        visit_begins_list.append(cur)
        cur = cur + timedelta(minutes=appointment_duration)
    for appointment in day_appointments:
        visit_begins_list.remove(appointment.start.replace(tzinfo=None))

    return visit_begins_list


# todo submit appointment

@api_view(["GET"])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_available_times(request):
    user = request.user
    if hasattr(user, 'patient'):
        # user = user.patient
        if request.GET.get('qid'):
            try:
                doctor = Doctor.objects.get(council_ID=request.GET.get('qid'))
            except:
                return Response({'message': 'No Such Doctor'}, status=400)
            if request.GET.get('qdate'):
                import datetime
                date = datetime.datetime.strptime(request.GET.get('qdate'), '%Y-%m-%d')
                result = find_available_times(doctor, date)
                if result:
                    return Response({'message': 'fetched successfully', 'available_times': result}, status=200)
                else:
                    return Response({'message': 'No available time in this day', 'available_times': []}, status=200)
            else:
                return Response({'message': 'please specify a date'}, status=400)

        else:
            return Response({'message': 'No Doctor Specified'}, status=400)
    else:
        return Response({'message': 'Boro Laashi'}, status=403)


@api_view(['POST'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def post_comment(request):
    user = request.user
    if hasattr(user, 'doctor'):
        return Response({'message': 'Doctors are forbidden from posting a comment'}, status=403)
    elif hasattr(user, 'patient'):
        user = user.patient
        if request.data.get('council_id'):
            try:
                doctor = Doctor.objects.get(council_ID=request.data.get('council_id'))
            except:
                return Response({'message': 'No Such Doctor'}, status=400)
            if doctor:
                Comment.objects.create(doctor=doctor, patient=user, text=request.data['text'])
                return Response("comment posted", status=200)
            else:
                return Response({'message': 'No Such Doctor'}, status=400)
        else:
            return Response({'message': 'No Doctor is Specified'}, status=400)

    else:
        return Response({'message': 'You are forbidden from posting a comment'}, status=403)


@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def add_to_favourite_doctors(request):
    user = request.user
    if hasattr(user, 'doctor'):
        return Response({'message': 'Doctors are forbidden from having favorite doctor!!'}, status=403)
    elif hasattr(user, 'patient'):
        user = user.patient
        if request.data.get('council_id'):
            try:
                doctor = Doctor.objects.get(council_ID=request.data.get('council_id'))
            except:
                return Response({'message': 'No Such Doctor'}, status=400)
            if doctor:
                user.favorite_doctors.add(doctor)
                return Response("doctor added", status=200)
            else:
                return Response({'message': 'No Such Doctor'}, status=400)
        else:
            return Response({'message': 'No Doctor is Specified'}, status=400)

    else:
        return Response({'message': 'You are forbidden from favoriting a doctor'}, status=403)



@api_view(['GET'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def get_users_favourite_doctors(request):
    patient = Patient.objects.get(id=request.user.id)
    doctors = patient.favorite_doctors.all()
    return Response(
        list(doctors.values('first_name', 'last_name', 'degree', 'specialty', 'city', 'phone', 'council_ID')),
        status=200)


#
# @api_view(['POST'])
# @login_required()
# @authentication_classes([CsrfExemptSessionAuthentication])
# def update_working_hours(request):
#     pass


@api_view(['POST'])
@login_required()
@authentication_classes([CsrfExemptSessionAuthentication])
def set_appointment(request):
    user = request.user
    if hasattr(user, 'doctor'):
        return Response({'message': 'Doctors are forbidden from setting appointments'}, status=403)
    elif hasattr(user, 'patient'):
        user = user.patient
        if request.data.get('council_id'):
            try:
                doctor = Doctor.objects.get(council_ID=request.data.get('council_id'))
            except:
                return Response({'message': 'No Such Doctor'}, status=400)
            if doctor:
                try:
                    visit_datetime = datetime.strptime(request.data.get('datetime'), '%Y-%m-%dT%H:%M:%S')

                except:
                    return Response({'message': 'Bad Time Format'}, status=400)

                if visit_datetime in find_available_times(doctor, visit_datetime):
                    appointment = Appointment.objects.create(doctor=doctor, start=visit_datetime, patient=user,
                                                             description=request.data.get('description', ''))
                    return Response({'message': 'appointment was set successfully', 'appointment': str(appointment)},
                                    status=201)
                else:
                    return Response({'message': 'Sorry, not available'}, status=406)

            else:
                return Response({'message': 'No Such Doctor'}, status=400)
        else:
            return Response({'message': 'No Doctor is Specified'}, status=400)

    else:
        return Response({'message': 'You are forbidden from posting a comment'}, status=403)
