from django.contrib import admin

# Register your models here.
from .models import *


class UserAdmin(admin.ModelAdmin):
    exclude = ('is_staff', 'is_superuser', 'groups', 'user_permissions')
    readonly_fields = ('last_login', 'date_joined')


class CommentAdmin(admin.ModelAdmin):
    list_display = ()
    list_filter = ()
    search_fields = ()


admin.site.register(Doctor, UserAdmin)
admin.site.register(Patient, UserAdmin)
admin.site.register(Appointment)
admin.site.register(AvailableTime)
admin.site.register(Comment)
