from django.contrib.auth import authenticate
from django.forms import PasswordInput
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from panel.models import Doctor, Patient, Comment
from panel.validators import validate_mail, is_email_valid


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'}
                                     , write_only=True)

    def validate(self, attrs):
        user = authenticate(username=attrs['username'], password=attrs['password'])

        if not user:
            raise serializers.ValidationError('Incorrect username or password.')

        if not user.is_active:
            raise serializers.ValidationError('User is disabled.')

        return user


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'}
                                     , write_only=True)
    is_doctor = serializers.NullBooleanField()
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField()
    phone = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    council_ID = serializers.CharField(required=False)
    specialty = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    degree = serializers.CharField(required=False)

    #
    def validate(self, attrs):
        if attrs.get('is_doctor'):
            if attrs.get('council_ID') and attrs.get('phone'):
                if attrs.get('email') and not is_email_valid(email=attrs.get('email')):
                    raise ValidationError('Invalid Email')
                if not attrs.get('phone').replace('+', '00').isnumeric():
                    raise ValidationError('wrong phone number')
                if not attrs.get('council_ID').isnumeric():
                    raise ValidationError('bad council ID')
                return attrs
        else:
            return attrs

    def create(self, validated_data):
        if validated_data.pop('is_doctor'):
            return Doctor.objects.create_user(username=validated_data.pop('username'),
                                              password=validated_data.pop('password'),
                                              **validated_data)
        else:
            return Patient.objects.create_user(username=validated_data.pop('username'),
                                               password=validated_data.pop('password'),
                                               **validated_data)


class PatientProfileSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    phone = serializers.CharField(required=False)

    def validate(self, attrs):
        if attrs.get('first_name') and not attrs.get('first_name').isalpha():
            raise ValidationError('name should be alphabetics only')
        if attrs.get('last_name') and not attrs.get('last_name').isalpha():
            raise ValidationError('name should be alphabetics only')
        if attrs.get('phone') and not attrs.get('phone').replace('+', '00').isnumeric():
            raise ValidationError('incorrect phone')
        return attrs

    def update(self, instance, validated_data):
        if validated_data.get('phone'):
            instance.phone = validated_data.get('phone')

        if validated_data.get('first_name'):
            instance.first_name = validated_data.get('first_name')

        if validated_data.get('last_name'):
            instance.last_name = validated_data.get('last_name')
        instance.save()
        return instance


class DoctorProfileSerializer(serializers.Serializer):
    name = serializers.CharField()
    last_name = serializers.CharField()
    phone = serializers.CharField()
    city = serializers.CharField()

    def validate(self, attrs):
        pass

    def update(self, instance, validated_data):
        pass


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment

        fields = ('patient', 'doctor', 'text',)
