from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, AbstractUser
from django.db import models


# Create your models here.

class WeekDays(models.TextChoices):
    SAT = "Saturday"
    SUN = "Sunday"
    MON = "Monday"
    TUE = "Tuesday"
    WED = "Wednesday"
    THU = "Thursday"
    FRI = "Friday"


appointment_duration = 30  # minutes


class MyUser(AbstractUser):
    email = models.CharField(max_length=255, null=True, blank=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255)

    REQUIRED_FIELDS = []


class Doctor(MyUser):
    phone = models.CharField(max_length=15)
    council_ID = models.CharField(max_length=10, unique=True)
    specialty = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    degree = models.CharField(max_length=255, blank=True, null=True)

    REQUIRED_FIELDS = ['is_doctor', 'phone', 'doctor_ID']

    class Meta:
        verbose_name = 'doctor'


class Patient(MyUser):
    phone = models.CharField(max_length=15, blank=True, null=True)
    favorite_doctors = models.ManyToManyField(Doctor, related_name='favorited_by', blank=True)

    class Meta:
        verbose_name = 'patient'


# class Appointment(models.Model):
#     """Contains info about appointment"""
#
#     class Meta:
#         unique_together = ('doctor', 'date', 'timeslot')
#
#     TIMESLOT_LIST = (
#         (0, '09:00 – 10:00'),
#         (1, '10:00 – 11:00'),
#         (2, '11:00 – 12:00'),
#         (3, '12:00 – 13:00'),
#         (4, '13:00 – 14:00'),
#         (5, '14:00 – 15:00'),
#         (6, '15:00 – 16:00'),
#         (7, '16:00 – 17:00'),
#         (8, '17:00 – 18:00'),
#     )
#
#     doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='doctor_assigned')
#     date = models.DateField()
#     timeslot = models.IntegerField(choices=TIMESLOT_LIST)
#     patient_name = models.CharField(max_length=60)
#     patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
#
#     def __str__(self):
#         return '{} {} {}. Patient: {}'.format(self.date, self.time, self.doctor, self.patient_name)
#
#     @property
#     def time(self):
#         return self.TIMESLOT_LIST[self.timeslot][1]


class Appointment(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    description = models.TextField(max_length=400)
    # day = models.CharField(choices=WeekDays.choices, max_length=255)
    # start_time = models.FloatField(blank=True, null=True)
    # end_time = models.FloatField(blank=True, null=True)
    start = models.DateTimeField()
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='appointments')

    class Meta:
        unique_together = ('doctor', 'start')

    def __str__(self):
        return "Appointment for {} by Dr {} on {}".format(self.patient, self.doctor.council_ID, self.start)


class AvailableTime(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='available_times')
    day = models.CharField(choices=WeekDays.choices, max_length=255)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)

    # duration = end_time - start_time

    class Meta:
        unique_together = ('doctor', 'day')


class Comment(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    text = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
